import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
     
    message = 'Hello, Angular! :-)'
    changed = false

    changeMessage () {
      this.message = this.changed ? 'Hello, Angular! :-)' : 'Welcome Angular! :-)'
      this.changed = !this.changed

    }

